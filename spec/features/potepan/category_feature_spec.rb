require 'rails_helper'

RSpec.feature "Potepan::Category", :type => :feature do
  let(:test_taxonomy) { create(:taxonomy) }
  let(:test_taxon) { create(:taxon, taxonomy: test_taxonomy) }
  let(:test_image) { create(:image) }
  let(:test_variant) { create(:variant, images: [test_image], is_master: true) }
  let!(:test_product) { create(:product, master: test_variant, taxons: [test_taxon]) }
  
  feature "GET potepan/categories/:id" do
    background do
      visit potepan_category_path(test_taxon.id)
    end

    scenario "適切な画像が表示されていること" do
      expect(page).to have_selector("img[src*='/spree/products/#{test_product.images.first.id}/large/#{test_product.images.first.attachment_file_name}']")
    end

    scenario "適切なカテゴリー名が表示されていること" do
      expect(page).to have_selector("h2", text: test_taxon.name)
    end

    scenario "適切な商品名が表示されていること" do
      expect(page).to have_selector("h5", text: test_product.name)
    end

    scenario "適切な価格が表示されていること" do
      expect(page).to have_selector("h3", text: test_product.price)
    end

    scenario "サイドバーに表示されているtaxonomy名が正しいこと" do
      expect(page).to have_selector("ul > li", text: test_taxonomy.name)
    end

    scenario "taxonomyを選択すると適切なカテゴリー名が表示されていること" do
      expect(page).to have_selector("ul > li", text: test_taxon.name)
    end

    scenario "サイドバーの商品数が正しいこと" do
      expect(page).to have_selector("ul > li", text: test_taxon.products.count)
    end

    scenario "トップバナーにある画像のリンク先が正しいこと" do
      click_on 'topbanner'
      expect(current_path).to eq potepan_path
    end

    scenario "LIGHT SECTIONにある「Home」のリンク先が正しいこと" do
      within('.pageHeader') do
        click_link "HOME"
      end
      expect(current_path).to eq potepan_path
    end

    scenario "商品画像のリンク先が正しいこと" do
      click_link test_product.name
      expect(current_path).to eq potepan_product_path(test_product.id)
    end
  end
end