require 'rails_helper'

RSpec.feature "Potepan::Products", :type => :feature do
  let(:test_image) { create(:image) }
  let(:test_variant) { create(:variant, images: [test_image], is_master: true) }
  let(:test_taxonomy) { create(:taxonomy) }
  let(:test_taxon) { create(:taxon, taxonomy: test_taxonomy) }
  let(:test_product) { create(:product, master: test_variant, taxons: [test_taxon]) }
  let(:related_image) { create(:image) }
  let(:related_variant) { create(:variant, images: [related_image], is_master: true) }
  let!(:related_product) { create(:product, master: related_variant, taxons: [test_taxon]) }

  feature "GET potepan/products/:id" do
    background do
      visit potepan_product_path(test_product.id)
    end

    scenario "適切な画像が表示されていること（大）" do
      expect(page).to have_selector(".carousel-inner > .item > img[src*='/spree/products/#{test_product.images.first.id}/large/#{test_product.images.first.attachment_file_name}']")
    end

    scenario "適切な画像が表示されていること（小）" do
      expect(page).to have_selector(".carousel-inner > .thumb > img[src*='/spree/products/#{test_product.images.first.id}/large/#{test_product.images.first.attachment_file_name}']")
    end

    scenario "適切な商品名が表示されていること" do
      expect(page).to have_selector("h2", text: test_product.name)
    end

    scenario "適切な価格が表示されていること" do
      expect(page).to have_selector("h3", text: test_product.price)
    end

    scenario "適切な商品説明が表示されていること" do
      expect(page).to have_selector("p", text: test_product.description)
    end

    feature "「一覧ページへ戻る」のリンク先が正しいこと" do
      context 'productがtaxonsを持つとき' do
        scenario "「一覧ページへ戻る」のリンク先がその商品の一覧ページであること" do
          click_link '一覧ページへ戻る'
          expect(current_path).to eq potepan_category_path(test_product.taxons[0].id)
        end
      end

      context 'productがtaxonsを持たないとき' do
        let(:test_product) { create(:product, master: test_variant, taxons: []) }

        scenario "「一覧ページへ戻る」のリンク先がホームであること" do
          click_link '一覧ページへ戻る'
          expect(current_path).to eq potepan_path
        end
      end
    end

    scenario "関連商品画像が表示されていること" do
      expect(page).to have_selector("img[src*='/spree/products/#{related_product.images.first.id}/large/#{related_product.images.first.attachment_file_name}']")
    end

    scenario "関連商品画像のリンク先が正しいこと" do
      click_on "products-img-#{related_product.id}"
      expect(current_path).to eq potepan_product_path(related_product.id)
    end

    scenario "適切な関連商品名が表示されていること" do
      expect(page).to have_selector("h5", text: related_product.name)
    end

    scenario "適切な関連商品価格が表示されていること" do
      expect(page).to have_selector("h3", text: related_product.price)
    end
  end
end
