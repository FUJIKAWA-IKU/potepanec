require 'rails_helper'

RSpec.describe "Potepan::Products", :type => :model do
  let(:test_taxonomy) { create(:taxonomy) }
  let(:test_taxon) { create(:taxon, taxonomy: test_taxonomy) }
  let!(:test_product) { create(:product, taxons: [test_taxon]) }
  let!(:related_products) { create_list(:product, 4, taxons: [test_taxon]) }
  let(:norelation_taxon) { create(:taxon) }
  let!(:norelation_product) { create(:product, taxons: [norelation_taxon]) }

  it "商品詳細に該当する商品は表示していないこと" do
    related_products.each do |related_product|
      expect(related_product).not_to eq test_product
    end
  end

  it "関連商品に重複が無いこと" do
    expect(related_products == related_products.uniq).to be true
  end

  it "関連しない商品は含まれないこと" do
    related_products.each do |related_product|
      expect(norelation_product).not_to eq related_product
    end
  end
end
