require 'rails_helper'

RSpec.describe "Sessions", type: :request do
  let(:test_image) { create(:image) }
  let(:test_variant) { create(:variant, images: [test_image], is_master: true) }
  let(:test_taxonomy) { create(:taxonomy) }
  let(:test_taxon) { create(:taxon, taxonomy: test_taxonomy) }
  let!(:test_product) { create(:product, master: test_variant, taxons: [test_taxon]) }

  describe "GET potepan/products/:id" do
    before do
      get potepan_category_path(test_taxon.id)
    end

    it "商品詳細ページのリクエストが成功すること" do
      expect(response).to have_http_status(200)
    end

    it "商品名を取得していること" do
      expect(response.body).to include test_product.name
    end

    it "価格を取得していること" do
      expect(response.body).to include test_product.price.to_s
    end

    it "taxonomy名を取得していること" do
      expect(response.body).to include test_taxonomy.name
    end

    it "taxon名を取得していること" do
      expect(response.body).to include test_taxon.name
    end
  end
end
