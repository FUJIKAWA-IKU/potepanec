require 'rails_helper'

RSpec.describe "Sessions", type: :request do
  let(:test_variant) { create(:variant, is_master: true) }
  let(:test_taxonomy) { create(:taxonomy) }
  let(:test_taxon) { create(:taxon, taxonomy: test_taxonomy) }
  let(:test_product) { create(:product, master: test_variant, taxons: [test_taxon]) }
  let(:related_variant) { create(:variant, is_master: true) }
  let!(:related_products) { create_list(:product, 5, master: related_variant, taxons: [test_taxon]) }

  describe "GET potepan/products/:id" do
    before do
      get potepan_product_path(test_product.id)
    end

    it "商品詳細ページのリクエストが成功すること" do
      expect(response).to have_http_status(200)
    end

    it "商品名を取得していること" do
      expect(response.body).to include test_product.name
    end

    it "価格を取得していること" do
      expect(response.body).to include test_product.price.to_s
    end

    it "商品説明を取得していること" do
      expect(response.body).to include test_product.description
    end

    it "関連商品名を取得していること" do
      expect(response.body).to include related_products.first.name
    end

    it "関連価格を取得していること" do
      expect(response.body).to include related_products.first.price.to_s
    end

    it "5つ生成した関連商品のうち、4つ取得していること" do
      related_products.first(4).each do |related_product|
        expect(response.body).to include related_product.name
      end
    end

    it "5つ生成した関連商品のうち、1つは個数の制限がかけられていること" do
      expect(response.body).not_to include related_products[4].name
    end
  end
end
