class Potepan::ProductsController < ApplicationController
  LIMITED_NUMBER = 4
  def show
    @product = Spree::Product.find(params[:id])
    @related_products = @product.related_products.includes(master: [:images, :default_price]).limit(LIMITED_NUMBER)
  end
end
